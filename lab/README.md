# Laboratory Specs


### What's inside
* `board-schematic.pdf` - reference to the board schematic when necessary.
* `lab-**-**.pdf` - laboratory specifications as uploaded in UvLE.

I cannot promise quick updates to this folder. Sorry! If there are lab modules missing, they're at UvLE.

### To the Instructors
> Hi there.
>
> I'm Charles. I place the lab modules here after a week of uploading. This ensures that no leakage occurs (as you've enabled section-specific visibility on modules at UvLE). If putting them publicly is not allowable, please notify me and I'll have them removed immediately. My only intention is to simply allow students easy access to these resources in order for them to reference these quickly if necessary. 
>
> Thanks!
>
> Charles