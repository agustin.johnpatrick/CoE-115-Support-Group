# Blank C Template
## For PIC24FJ64GB002 and PIC24FJ64GA002

> *tl;dr*: If you're in the lab, you'll want to use `template_gb_clean.c`.

### What's inside
* `template.c` - includes block specific information about the template. Use this if you want guidance while building your code.
* `template_ga.c` - includes only `PIC24FJ64GA002` specific lines (this PIC is the one used last year).
* `template_gb.c` - includes only `PIC24FJ64GB002` specific lines (this PIC is the one used this year; **you probably want to use this one**).

These three files include explanations for essential parts of the code. If you're confident with your C skills and want to start without a bunch of comments you don't need anymore, use the corresponding `*_clean.c` which strips most comments out.

Pick one that suits your needs, and start coding.

### Protip
These templates include the configuration bit for debugging, `ICS_PGx2`, so that you can debug your code on the board. (_Thanks, Carl!_)