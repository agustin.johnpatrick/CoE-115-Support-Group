/*
 * File:   your file here
 * Author: your name here
 *
 */
 
#include "xc.h"

#pragma config FWDTEN = OFF
#pragma config JTAGEN = OFF
#pragma config POSCMOD = NONE
#pragma config OSCIOFNC = ON
#pragma config FCKSM = CSDCMD
#pragma config FNOSC = FRCPLL
#pragma config ICS = PGx2

// Disable these config bits when using the PIC24FJ64GA002
// Leave them on when using the PIC24FJ64GB002
#pragma config PLL96MHZ = OFF
#pragma config PLLDIV = NODIV
#pragma config SOSCSEL = IO

int main (void) {
    // Enable this when using the PIC24FJ64GA002.
    // This ensures that your code is running in 2 MHz (Fcy).
    // CLKDIV = 0x0300;

    return 0;
}