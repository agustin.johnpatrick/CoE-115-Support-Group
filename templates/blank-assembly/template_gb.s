;; *********************
;; File:   your file here
;; Author: your name here
;; Use with PIC24FJ64GB002.
;; *********************

.include "p24FJ64GB002.inc"

.global __reset

config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL & PLL96MHZ_OFF & PLLDIV_NODIV
    
.bss
;; Define your file registers here. (e.g., "i: .space 2")

.text
__reset:
    mov #__SP_init, W15       ;; Initialize Stack Pointer (W15)
    mov #__SPLIM_init, W0
    mov W0, SPLIM             ;; Initialize Stack Limit Register

    ;; Clear your file registers here. (e.g., "clr i")

    ;; Setup your initial registers here (ie., TRISx, LATx, AD1PCFG, etc.)

main:
    ;; Code proper starts here.
    

;; This label stops your code after execution.
;; 
;; This also prevents your main code to go and execute 
;; subroutines you've created.
;; 
;; You may freely remove this if necessary.

done:
    bra done


;; Here's a one second delay for your convenience.
delay1s:
    mov #0x000b, W0
    mov W0, i 
    mov #0x2c23, W0
    mov W0, j

loop1s:
    dec j 
    bra nz, loop1s
    dec i 
    bra nz, loop1s 
    return

.end
