# Templates


### What's inside
* `blank-assembly/` - a blank template for Assembly
* `blank-c/` - a blank template for C
* `blink/` - a sample code making both `LED1` and `LED2` blink
* `pushbutton/` - a sample code using `PB` and `LED1`

### To the Instructors
> Hi there.
>
> I'm Charles. I provide these sample codes to takers. They come from the provided examples from the previous labs, and were modified to 
> 1. allow debugging,
> 2. ensure that it works with both the `PIC24FJ64GB002` and `PIC24FJ64GA002`, 
> 3. have comments explaining how the code works, and 
> 4. provide a jumpstart for students for their code.
>
> No full code implementations or answers for the previous labs are included. I do not condone cheating as much as you guys do, and these only serve as templates for further work done by the students themselves. If there are any comments or suggestions, please do notify me and I will comply accordingly.
>
> Thanks!
>
> Charles