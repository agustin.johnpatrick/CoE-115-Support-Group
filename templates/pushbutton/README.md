# Sample Code: Pushbutton (Assembly)
## For PIC24FJ64GB002 and PIC24FJ64GA002

> *tl;dr*: Turns on LED when Pushbutton is pressed, off otherwise. Easy.

### What's inside
* `pushbutton.s` - a pushbutton code written in Assembly, with a bunch of comments explaining how stuff works.
* `pushbutton_clean.s` - same pushbutton code without most of the comments.

If you're using the `PIC24FJ64GB002`, the provided code works as is, but if you're using the `PIC24FJ64GA002`, (the PIC from the previous batches) you may have to enable and disable some lines as noted.

Pick one that suits your needs, and start coding.

### Prerequisites
* `LED1` has a jumper connected to `RA0`
* `PB` has a jumper connected to `RB0`

### Protip
Use this as a bootstrap for your laboratory code, or use this as a sanity check to test your onboard pushbutton.

### What's next?
Try setting up a pushbutton circuit (see Lab 1) to another pin and try modifying the code to make it work.