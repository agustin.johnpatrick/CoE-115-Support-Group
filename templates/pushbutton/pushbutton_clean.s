;; *********************
;; pushbutton.s
;;    This turns LED1 (RA0) on when PB (RB0) is pushed down (shorted).
;;    No debounce is implemented in this code.
;;    Connect LED1 and PB jumpers.
;; *********************

;; Select which board you're using
.include "p24FJ64GB002.inc"
; .include "p24FJ64GA002.inc"

.global __reset

;; For the PIC24FJ64GB002, enable these config bits.
config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL & PLL96MHZ_OFF & PLLDIV_NODIV

;; For the PIC24FJ64GA002, enable these config bits.
; config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
; config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL

.bss
 
.text
__reset:
    mov #__SP_init, W15
    mov #__SPLIM_init, W0
    mov W0, SPLIM

    ;; For the PIC24FJ64GA002, enable this register write.
    ;; This ensures that your code is running in 2 MHz.
    ; mov #0x0300, W0
    ; mov W0, CLKDIV

    mov #0xFFFF, W0
    mov W0, AD1PCFG

    mov #0xFFFE, W0
    mov W0, TRISA

    mov #0xFFFF, W0
    mov W0, TRISB
 
main:
waitdown:
    bset LATA, #0

    btsc PORTB, #0
    goto waitdown 
 
waitup:
	bclr LATA, #0

	btss PORTB, #0
	goto waitup

	goto main
 
.end
