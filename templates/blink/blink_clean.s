;; *********************
;; blink.s
;;    This blinks LED1 (RA0) and LED2 (RA1) on the board for one second.
;;    Connect LED1 and LED2 jumpers.
;; *********************

;; Select which board you're using
.include "p24FJ64GB002.inc"
; .include "p24FJ64GA002.inc"

.global __reset

;; For the PIC24FJ64GB002, enable these config bits.
config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL & PLL96MHZ_OFF & PLLDIV_NODIV

;; For the PIC24FJ64GA002, enable these config bits.
; config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
; config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL

.bss
i: .space 2
j: .space 2

.text
__reset:
    mov #__SP_init, W15
    mov #__SPLIM_init, W0
    mov W0, SPLIM

    clr i 
    clr j

    ;; For the PIC24FJ64GA002, enable this register write.
    ;; This ensures that your code is running in 2 MHz.
    ; mov #0x0300, W0
    ; mov W0, CLKDIV

    mov #0xFFFC, W0
    mov W0, TRISA

    mov #0x0003, W1

main:
    clr LATA
    call delay1s
    mov W1, LATA
    call delay1s

    bra main

delay1s:
    mov #0x000b, W0
    mov W0, i 
    mov #0x2c23, W0
    mov W0, j

loop1s:
    dec j 
    bra nz, loop1s
    dec i 
    bra nz, loop1s 
    return

.end
