;; *********************
;; blink.s
;;    This blinks LED1 (RA0) and LED2 (RA1) on the board for one second.
;;    Connect LED1 and LED2 jumpers.
;; *********************

;; Select which board you're using
.include "p24FJ64GB002.inc"
; .include "p24FJ64GA002.inc"

.global __reset

;; For the PIC24FJ64GB002, enable these config bits.
config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL & PLL96MHZ_OFF & PLLDIV_NODIV

;; For the PIC24FJ64GA002, enable these config bits.
; config __CONFIG1, FWDTEN_OFF & JTAGEN_OFF & ICS_PGx2
; config __CONFIG2, POSCMOD_NONE & OSCIOFNC_OFF & FCKSM_CSDCMD & FNOSC_FRCPLL

.bss
;; Allocate 2 bytes for file registers i, j.
i: .space 2
j: .space 2

.text
__reset:
    ;; Initialize the stack pointer register and stack pointer limit register.
    mov #__SP_init, W15
    mov #__SPLIM_init, W0
    mov W0, SPLIM

    ;; Clear file registers i, j.
    clr i 
    clr j

    ;; For the PIC24FJ64GA002, enable this register write.
    ;; This ensures that your code is running in 2 MHz.
    ; mov #0x0300, W0
    ; mov W0, CLKDIV

    ;; Set RA0 (connected to LED 1) and RA1 (connected to LED 2) to output.
    ;; Setting a TRIS bit to 1 makes a pin an input, 0 an output.
    ;; 0xFFFC is (1111 1111 1111 1100) in binary, and bits 1 and 0 (LSB)
    ;;    correspond to RA1 and RA0 respectively.

    mov #0xFFFC, W0
    mov W0, TRISA

    ;; We place 0x0003 to register W1 for future use.
    ;; 0x0003 is (0000 0000 0000 0011) in binary, corresponding to
    ;;    the first two pins (RA1, RA0) in port A.
    mov #0x0003, W1

main:
    ;; We have set up our LEDs such that it turns on when the pin
    ;;    is set to low (GND). This is because we have configured our
    ;;    circuit to be VDD -> R -> LED -> RAx (pin). Setting the pin
    ;;    to high (VDD) prevents current from flowing through, and
    ;;    setting this to low (GND) allows a voltage drop across the LED,
    ;;    turning it on. The main purpose of the resistor is to limit the
    ;;    current across the branch, so as to protect the microcontroller.

    ;; Set all pins of port A to low (GND).
    ;; This turns on the LEDs.
    clr LATA

    ;; Call the delay subroutine.
    call delay1s

    ;; Write 0x0003 to LATA. This effectively sets RA1 and RA0
    ;;    to high (VDD), and everything else to low (GND).
    mov W1, LATA


    ;; Call the delay subroutine.
    call delay1s

    ;; Loop back.
    bra main

delay1s:
    mov #0x000b, W0
    mov W0, i 
    mov #0x2c23, W0
    mov W0, j

loop1s:
    dec j 
    bra nz, loop1s
    dec i 
    bra nz, loop1s 
    return

.end
