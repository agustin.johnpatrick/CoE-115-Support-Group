# Sample Code: Blink (Assembly)
## For PIC24FJ64GB002 and PIC24FJ64GA002

> *tl;dr*: Blinks on for a second, blinks off for a second. Easy.

### What's inside
* `blink.s` - a blink code written in Assembly, with a bunch of comments explaining how stuff works.
* `blink_clean.s` - same blink code without most of the comments.

If you're using the `PIC24FJ64GB002`, the provided code works as is, but if you're using the `PIC24FJ64GA002`, (the PIC from the previous batches) you may have to enable and disable some lines as noted.

Pick one that suits your needs, and start coding.

### Prerequisites
* `LED1` has a jumper connected to `RA0`
* `LED2` has a jumper connected to `RA1`

### Protip
Use this as a bootstrap for your laboratory code, or use this as a sanity check to test whether or not your board or IC works.

### What's next?
Try setting up an LED circuit (see Lab 1) to another pin and try modifying the code to make it work.